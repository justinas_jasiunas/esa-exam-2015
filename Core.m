function [cost_total, T, p, less, lessEq] = Core(decisionVariable)

global CO2Price;

% Known variables ---------------------------------------------------------
% Temperatures in K
T0   = 273.15 + 25;
T    = zeros(4,1);
T(1) = T0;

% Pressures in bar
p0   = 1;
p    = zeros(4,1);
p(1) = p0;
p(4) = p0;

% Thermodymic properties
cp_air   = 1.004;       % kJ/kgK
cp_gas   = 1.17;        % kJ/kgK
gama_air = 1.4;
gama_gas = 1.33;
LHV      = 50000;       % kJ/kg

% Power plant
P_el         = 5000;    % kW
Ef_generator = 0.98;

% Economical parameters
c11 = 39.5;             % $/(kg/s)
c12 = 0.9;
c21 = 25.6;              % $/(kg/s)
c22 = 0.995;
c23 = 0.018;            % 1/K
c24 = 26.4;
c31 = 266.3;            % $/(kg/s)
c32 = 0.92;
c33 = 0.036;            % 1/K
c34 = 54.4;
CRF = 0.156;
FuelCost          = 3 * 10^-6;      % $/kJ
OperationalHours  = 6500;
MaintananceFactor = 1.05;
PollutionPenalty  = 1;

% Decision variables ------------------------------------------------------
beta        = decisionVariable(1);
IzEf_comp   = decisionVariable(2);
rb          = decisionVariable(3);
T(3)        = decisionVariable(4);
IzEf_turb   = decisionVariable(5);

% Dependent variables -----------------------------------------------------
% Copresor analysis
p(2) = p0 * beta;
power_air = (gama_air - 1) / gama_air;
T(2) = T0 * (1 + (beta ^ power_air - 1) / IzEf_comp);

% Combustor analysis
p(3) = p(2) * rb;

% Turbine analysis
rT = p(3) / p(4);
power_gas = (1 - gama_gas) / gama_gas;
T(4) = T(3) * (1 - IzEf_turb * (1 - rT ^ power_gas));

% Mass flows in kg/s
f = (cp_gas * ( T(3) - T0 ) - cp_air * ( T(2) - T0 )) / (LHV - cp_gas * (T(3) - T0));
denominator = (1 + f) * cp_gas * (T(3) - T(4)) - cp_air * (T(2) - T0);
mf_air = P_el / (Ef_generator * denominator);
mf_fuel = f * mf_air;
mf_gas  = (1 + f) * mf_air;
mf_CO2 = mf_fuel * 44 / 16;

% Annual cost -------------------------------------------------------------
cost_comp = (c11 * mf_air) / (c12 - IzEf_comp) * beta * log(beta);
cost_comb = (c21 * mf_air) / (c22 - rb) * (1 + exp(c23 * T(3) -c24));
cost_turb = (c31 * mf_gas) / (c32 - IzEf_turb) * log(rT) * (1 + exp(c33* T(3) - c34));
cost_inv  = (cost_comp + cost_comb + cost_turb) * CRF * MaintananceFactor;

SecondsPerHour = 3600;
cost_CO2  = mf_CO2 * CO2Price * PollutionPenalty * OperationalHours * SecondsPerHour;

cost_fuel = mf_fuel * LHV * FuelCost * SecondsPerHour * OperationalHours;

cost_total = cost_inv + cost_CO2 + cost_fuel;

% Constrains --------------------------------------------------------------
less(1) = T(1) - T(2);
less(2) = T(2) - T(3);
less(3) = T(4) - T(3);


lessEq = [];
end