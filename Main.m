close all;
clc;
clear all;

global CO2Price;

% Decision variables: beta, IzEf.comp, Pr.comb_dp, T3, IzEf.exp
lowerBoundary = [ 8 0.70 0.90 1200 0.86];
initialValue  = [15 0.87 0.95 1400 0.89];
upperBoundary = [20 0.89 0.99 1600 0.91];

% Initial optimization
CO2Price          = 0.0071;         % $/kJ

OptimizedDecisionVariables1 = fmincon(@AnnualCost, initialValue, [], [], [], [], lowerBoundary, upperBoundary, @Constraints);
cost1 = AnnualCost(initialValue);

% Sensitivity of CO2price analysis 
for i = 1:10
    CO2Price = 0.005 * i;           % $/kJ
    OptimizedDecisionVariables = fmincon(@AnnualCost, initialValue, [], [], [], [], lowerBoundary, upperBoundary, @Constraints);
    cost = AnnualCost(initialValue);
    
    CO2PriceMatrix(i) = CO2Price;
    OptimizedDecisionVariablesMatrix(:, i) = OptimizedDecisionVariables;
    costMatrix(i) = cost;
end

NewMatrix = [CO2PriceMatrix' costMatrix' OptimizedDecisionVariablesMatrix'];